<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="css/style.css">
    </head>
    <body class='home'>
      <main>

        <section class="main-left">
          <img class='logo' src="img/portfolio logo.svg" alt="Portfolio logo">

          <ul class='contact-info-text'>
            <h1>jacquelyn bobbi<br><hr>bortolussi</h1>
            <li>1 604 789 0443</li>
            <li>bobbi@uvic.ca</li>
          </ul>

          <nav>
            <ul>
              <a href=''><li class='active'>home</li></a>
              <a href='page-templates/about.html'><li>about</li></a>
              <a href=''><li>projects</li></a>
            </ul>
          </nav>

        </section>
        <section class="main-right">
          <h2>If you didn’t find<br>what you’re<br>looking for, </h2>
          <h2>feel free<br>to toss this<br>into a ♻ bin.</h2>
          <p>Jacquelyn Bortolussi, a visual artist and photographer, received a Bachelor of Fine Arts with distinction at the University of Victoria and participated in a summer studio course concentrating in architecture at the Harvard Graduate School of Design. Previously she worked at the artist artist-run centre Open Space, where she supported and documented numerous events in visual arts, new music, new media, literature, and performance art. She currently studies architectural history at the University of British Columbia, is an active member of the Vancouver arts collective Them;, and is a board member of the Ministry of Casual Living. </p>
        </section>
      </main>
    </body>
</html>
