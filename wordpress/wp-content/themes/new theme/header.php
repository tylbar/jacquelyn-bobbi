<?php
/**
 *  Theme: Jacquelyn
 *  File: header.php
 *  Author: Tyler Barnes
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo("charset"); ?>" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" type="image/png" href="<?php bloginfo('url');?>/favicon.ico">
		<title><?php wp_title(''); ?></title>
		<?php wp_head(); ?>
	</head>
<body <?php body_class(); ?>>

		<main role="main">
