<?php
/**
 *  Theme: Jacquelyn
 *  File: front-page.php
 *  Author: Tyler Barnes
 */

get_header(); ?>

<section class="main-left">
	<img class='logo' src="<?php the_field('logo', 'option'); ?>" alt="Portfolio logo">

	<ul class='contact-info-text'>
		<h1><?php the_field('your_name_above_line', 'option'); ?><br><hr><?php the_field('your_name_below_line', 'option'); ?></h1>
		<?php

		// check if the repeater field has rows of data
		if( have_rows('contact_information', 'option') ):

		 	// loop through the rows of data
		    while ( have_rows('contact_information', 'option') ) : the_row();
		?>
			<li>
						<?php
		        // display a sub field value
		        the_sub_field('contact_line');
						?>
			</li>
		<?php

		    endwhile;

		else :

		    // no rows found

		endif;

		?>
	</ul>

	<?php wp_nav_menu( array( 'container' => 'nav','container_id' => 'mainNav','container_class' => 'top-nav','theme_location' => 'main','menu_class'=>'list-inline' ) ); ?>

</section>
<section class="main-right">
	<h2><?php the_field('back_title_top'); ?></h2>
	<h2><?php the_field('back_title_bottom'); ?></h2>
	<p><?php the_field('paragraph_text'); ?></p>
</section>

<?php get_footer(); ?>
