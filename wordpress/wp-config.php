<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'jacquelyn');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '8VnmwlZXX/<Az?]3La*icJF!{(3J ZhbUR?:TRV&(hpP=8J:Q%==8:.xky8/e9 %');
define('SECURE_AUTH_KEY',  '3PLLi%v;Hfzm,HU8^mThYswrn]]ltylmXg`KS26 4()F%02C72dN@I?.@R7PQIkq');
define('LOGGED_IN_KEY',    'n?&folFblj#ALiz92A,alxQR&c@W{O?l#9fH?Zw61zlYDl1zBDr1c{gvMBq!lqAl');
define('NONCE_KEY',        '4f|1kEF.s>`&r2g(=%)byXb[v+XG im%<x!kVQp6Olg!mPn]Os)7$++/Ec/#mIjb');
define('AUTH_SALT',        'M#uWOO5O[]|orrV5>r:(;/4Ef8?PXpW[0?lO|~IwLkxL$5B.*??3a8]sQXv2EMQ?');
define('SECURE_AUTH_SALT', 'x7I~JIn~ C(.,q)GUqWro.)]&1(;PHgwN4Pn><TiE;r4MHu2 ;N?#HNh(nCIj!5N');
define('LOGGED_IN_SALT',   'ttfp<;m]*hLn~<(p CSICZ$%VCiiPsh;z.ACecbMJv2=Xi{ug@}NwLeQ(Z:$fFj]');
define('NONCE_SALT',       'X45.T[3F=~DK*]k?59{q!EEbju/%V)@@sR)!%WhJ*r4[b.xndIOoHHQx|(Ir$2*H');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
